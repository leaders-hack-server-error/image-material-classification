import os
from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename

from PIL import Image
import torchvision.transforms as T


import requests
import base64
import torch
import torchvision
from transformers import AutoFeatureExtractor, ResNetForImageClassification, ResNetModel
from PIL import Image

import pickle

with open("clusters_wall.pkl", "rb") as f:
    model_wall = pickle.load(f)
class_names_wall = []
class_centers = []
for class_name, class_center in model_wall.items():
    class_names_wall.append(class_name)
    class_centers.append(class_center)
class_centers_wall = torch.cat(class_centers)

sim = torch.nn.CosineSimilarity()

feature_extractor = AutoFeatureExtractor.from_pretrained("microsoft/resnet-18")
backbone = ResNetModel.from_pretrained("microsoft/resnet-18")

app = Flask(__name__)

@app.route('/')
def hello_geek():
    return '<h1>Hello from Flask & Docker</h2>'


def get_flat_number(img) -> int:
    """
    Идем в ocr и получаем число которое видим на экране. Если номер не определяется то мы уже внутри квартиры
    """
    r = requests.post("http://84.252.138.252:5898/recognize_nameplate", json={"image": encoded_string})
    r = r.json()["result"]
    if r == '':
        return 0
    else:
        return int(r)



@app.route('/classify', methods=['POST'])
def upload():
    file = request.files['image']
    # Read the image via file.stream
    img = Image.open(file.stream)
    img_encoded = base64.b64encode(img).decode('utf-8')
    flat_number = get_flat_number(img_encoded)
    if flat_number == 0:
        #TOOD: Create new flat in DB
        pass
    else:
        # Пытаемся распознать что на кадре и отнести это к последнему номеру квартиры что видим
        transform = T.PILToTensor()
        image = transform(img)
        image = image[:3, :, :]

        inputs = feature_extractor(image, return_tensors="pt")
        feature_vector = backbone(inputs["pixel_values"]).pooler_output.squeeze()

        cosines = sim(feature_vector, class_centers_wall)
        argmax = cosines.argmax().item()
        matched_class = class_names_wall[argmax]


        #TODO: Кладем в базу информацию о том что распознали и в какое время
        return jsonify({'msg': 'success', 'size': [img.width, img.height], "tensor_size": feature_vector.shape, "cosines": str(cosines), "matched_class": matched_class})


if __name__ == "__main__":
    app.run(debug=True)