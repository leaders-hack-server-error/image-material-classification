FROM image_classification:base

WORKDIR /python-docker

COPY . .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]